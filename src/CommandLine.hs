module CommandLine
  ( CommandLine(..)
  , Mode(..)
  , mode
  , args
  , enableTiming
  )
where

import           Options.Applicative

data Mode =
      OneShot String -- | try a single url and exit
    | ShowHelp  -- | show help and exit
    | ShowVersion -- | show version and exit
    | Normal FilePath -- | rank a list of mirrors
    deriving (Show)

data CommandLine =
  CommandLine
    { showVersion :: Bool
    , showHelp :: Bool
    , numbers :: Int
    , max :: Int
    , showTimes :: Bool
    , repoUrl :: Bool
    , verbose :: Bool
    , repo :: Maybe String
    , target :: String
    -- todo how many to do at once
    } deriving (Show)

enableTiming :: CommandLine -> CommandLine
enableTiming cli = cli { showTimes = True }

args :: Parser CommandLine
args =
  CommandLine
    <$> switch showVersion'
    <*> switch showHelp'
    <*> option auto numbers'
    <*> option auto max'
    <*> switch showTimes'
    <*> switch repoUrl'
    <*> switch verbose'
    <*> option auto repo'
    <*> argument str target'
 where
  showVersion' =
    mconcat [long "version", help "show programs version number and exit"]
  showHelp' =
    mconcat [long "help", short 'h', help "show this help message and exit"]
  numbers' = mconcat
    [ value 10
    , short 'n'
    , metavar "NUM"
    , help "number of servers to output, 0 for all"
    ]
  max' = mconcat
    [ value 30
    , short 'm'
    , long "max-time"
    , metavar "NUM"
    , help "specify a ranking operation timeout, can be a decimal number"
    ]
  showTimes' = mconcat
    [ short 't'
    , long "times"
    , help "only output mirrors and their response times"
    ]
  repoUrl' = mconcat [short 'u', long "url", help "test a specific URL"]
  verbose' = mconcat [short 'v', long "verbose", help "be verbose in output"]
  repo'    = mconcat
    [ value Nothing
    , short 'r'
    , long "repo"
    , help "specify a repository name instead of guessing"
    ]
  target' = metavar "FILE|URL"

mode :: CommandLine -> Mode
mode cli | showVersion cli = ShowVersion
         | showHelp cli    = ShowHelp
         | repoUrl cli     = OneShot (target cli)
         | otherwise       = Normal (target cli)
