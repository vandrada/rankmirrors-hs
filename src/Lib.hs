{-# LANGUAGE RankNTypes #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module Lib
  ( RankMirrorsM
  , Mirror(unMirror)
  , Results(unResults)
  , Result
  , withRankMirrorsM
  , runRankMirrorsM
  , mirrorResponse
  , responseTime
  , url
  , mkMirror
  , tryMirrors
  , evaluate
  , evaluateAndTake
  , top
  , mirror
  )
where

import           Control.Monad.Catch            ( MonadCatch
                                                , MonadThrow
                                                , catch
                                                )
import           Control.Exception              ( SomeException )
import           Data.Maybe                     ( isJust
                                                , fromJust
                                                )
import           Control.Monad.Reader
import           Data.Text                      ( Text )
import           Data.List                      ( sortOn )
import qualified Data.Text                     as T
import           Data.Int                       ( Int64 )
import           Control.Monad                  ( void )
import qualified Network.Wreq                  as W
import           Network.HTTP.Client            ( defaultManagerSettings
                                                , managerResponseTimeout
                                                , responseTimeoutMicro
                                                )
import           Network.HTTP.Client.TLS        ( tlsManagerSettings )
import           Control.Lens
import           System.Process                 ( readProcess )
import           System.Clock
import           Control.Concurrent.Async       ( mapConcurrently )

import           CommandLine

--------------------------------------------------------------------------------
-- Types
--------------------------------------------------------------------------------
-- nullary types to tag 'Results' with
data Evaluated
data Unevaluated

-- Our base monad
newtype RankMirrorsM a = RankMirrorsM { unRankMirrorsM :: ReaderT CommandLine IO a }
  deriving (Functor, Applicative, Monad, MonadIO, MonadReader CommandLine, MonadCatch, MonadThrow)

runRankMirrorsM :: RankMirrorsM a -> CommandLine -> IO a
runRankMirrorsM = runReaderT . unRankMirrorsM

withRankMirrorsM
  :: (CommandLine -> CommandLine) -> RankMirrorsM a -> RankMirrorsM a
withRankMirrorsM f = RankMirrorsM . withReaderT f . unRankMirrorsM

-- | A mirror to try
newtype Mirror = Mirror { unMirror :: Text } deriving (Show, Ord, Eq)

url :: Mirror -> String
url = T.unpack . unMirror

-- | Smart constructor for @Mirror@, expands $arch and $repo if necessary
mkMirror :: (MonadIO m) => String -> m Mirror
mkMirror mirror' = do
  arch <- liftIO $ T.strip . T.pack <$> readProcess "uname" ["-m"] ""
  pure . Mirror . subArch arch . subRepo . T.pack $ mirror'
 where
  subArch :: Text -> Text -> Text
  subArch = T.replace "$arch"
  subRepo :: Text -> Text
  subRepo = T.replace "$repo" "core"

-- | The relevant bits we care about when trying a @Mirror@
newtype MirrorResponse =
  MirrorResponse { unMirrorResponse :: (Int, Double) }
    deriving (Show)

mkMirrorResponse :: Int -> Double -> MirrorResponse
mkMirrorResponse status time = MirrorResponse (status, time)

statusCode :: Lens' MirrorResponse Int
statusCode = lens getter setter
 where
  getter r = unMirrorResponse r ^. _1
  setter r n = MirrorResponse $ unMirrorResponse r & _1 .~ n

responseTime :: Lens' MirrorResponse Double
responseTime = lens getter setter
 where
  getter r = unMirrorResponse r ^. _2
  setter r n = MirrorResponse $ unMirrorResponse r & _2 .~ n

-- The results of evaluating a @Mirror@
newtype Result = Result { unResult :: (Mirror, MirrorResponse) }
  deriving (Show)

mkResult :: Mirror -> MirrorResponse -> Result
mkResult mirror response = Result (mirror, response)

mirror :: Lens' Result Mirror
mirror = lens getter setter
 where
  getter result = unResult result ^. _1
  setter result new = Result $ unResult result & _1 .~ new

mirrorResponse :: Lens' Result MirrorResponse
mirrorResponse = lens getter setter
 where
  getter result = unResult result ^. _2
  setter result new = Result $ unResult result & _2 .~ new

-- | Collection of @Result@ with a tag indicating whether it's been evaluated
newtype Results a = Results { unResults :: [Result] }
  deriving (Show)

mkResults :: [Result] -> Results a
mkResults = Results

--------------------------------------------------------------------------------
-- Functions
--------------------------------------------------------------------------------
tryMirror :: (MonadIO m, MonadCatch m) => Mirror -> m (Maybe Result)
tryMirror mirror = do
  resp <- (Just <$> fetchUrl mirror) `catch` handler
  pure $ mkResult mirror <$> resp
 where
  handler :: (MonadIO m) => SomeException -> m (Maybe MirrorResponse)
  handler _ = pure Nothing

tryMirrors :: (MonadIO m) => [Mirror] -> m (Results Unevaluated)
tryMirrors mirrors =
  mkResults' <$> mapM (liftIO . mapConcurrently tryMirror) chunks
 where
  chunks :: [[Mirror]]
  chunks = mirrors `chunksOf` 10

  mkResults' :: [[Maybe Result]] -> Results Unevaluated
  mkResults' = mkResults . map fromJust . filter isJust . concat

-- | Evaluates the list of results and returns the fastest ones
evaluate :: Results Unevaluated -> Results Evaluated
evaluate results =
  mkResults . sortOn (\m -> m ^. mirrorResponse . responseTime) $ unResults
    results

evaluateAndTake :: Int -> [Mirror] -> RankMirrorsM (Results Evaluated)
evaluateAndTake n mirrors = fmap (top n . evaluate) $ tryMirrors mirrors

-- | takes the top 'n' results from a list of evaluated results
top :: Int -> Results Evaluated -> Results Evaluated
top n = mkResults . take n . unResults

-- | fetches a URL and times the response
fetchUrl :: (MonadIO m) => Mirror -> m MirrorResponse
fetchUrl mirror = do
  (resp, time) <-
    liftIO $ timeIt . W.getWith opts . T.unpack . unMirror $ mirror
  pure $ mkMirrorResponse (resp ^. W.responseStatus . W.statusCode) time
 where
  opts = W.defaults & W.manager .~ Left
    (tlsManagerSettings
      { managerResponseTimeout = responseTimeoutMicro $ 1000000 * 30
      }
    )

chunksOf :: [a] -> Int -> [[a]]
chunksOf xs n = reverse $ go xs n []
 where
  go [] _ acc = acc
  go xs n acc =
    let (taken, dropped) = splitAt n xs in go dropped n (taken : acc)

timeIt :: (MonadIO m) => m a -> m (a, Double)
timeIt action = do
  start <- liftIO $ getTime clock
  res   <- action
  end   <- liftIO $ getTime clock
  pure (res, asDouble (diffTimeSpec end start))
 where
  asDouble :: TimeSpec -> Double
  asDouble = realToFrac . toNanoSecs

  clock :: Clock
  clock = Realtime
