{-# LANGUAGE OverloadedStrings #-}
module Main where

import           Control.Monad                  ( forM_ )
import           Control.Lens
import           Control.Monad.IO.Class         ( liftIO )
import           Control.Monad.Reader           ( ask
                                                , withReaderT
                                                )
import           Data.List                      ( isPrefixOf )
import qualified Options.Applicative           as O
import           Options.Applicative            ( (<**>) )

import           CommandLine
import           Lib

version :: String
version = "0.1"

main :: IO ()
main = do
  opts' <- O.execParser opts
  print opts'
  runRankMirrorsM (run (mode opts')) opts'
 where
  opts = O.info
    (args <**> O.helper)
    (O.fullDesc <> O.header ("rankmirrors " <> version) <> O.progDesc
      "Rank pacman mirrors"
    )

run :: Mode -> RankMirrorsM ()
run ShowHelp      = liftIO $ putStrLn "helo"
run ShowVersion   = liftIO $ putStrLn version
run (OneShot url) = withRankMirrorsM enableTiming $ do
  mirror  <- mkMirror url
  results <- evaluateAndTake 1 [mirror]
  printResults results
run (Normal file) = do
  mirrors <- getMirrorUrls file >>= mapM mkMirror
  results <- evaluateAndTake 10 mirrors
  printResults results

printResults :: Results a -> RankMirrorsM ()
printResults = mapM_ showResults . unResults

showResults :: Result -> RankMirrorsM ()
showResults result = do
  cli <- ask
  if showTimes cli
    then liftIO . putStrLn $ unwords
      [ url (result ^. mirror)
      , ":"
      , show (result ^. mirrorResponse . responseTime)
      ]
    else liftIO . putStrLn $ url (result ^. mirror)

getMirrorUrls :: FilePath -> RankMirrorsM [String]
getMirrorUrls file = fmap (dropServerPrefix . dropComments . lines)
  $ liftIO (readFile file)
 where
  dropComments     = filter (isPrefixOf "Server = ")
  dropServerPrefix = map (dropPrefix "Server = ")

dropPrefix :: (Eq a) => [a] -> [a] -> [a]
dropPrefix prefix xs | prefix `isPrefixOf` xs = drop' prefix xs
                     | otherwise              = xs
 where
  drop' _        []       = []
  drop' []       xs       = xs
  drop' (c : cs) (x : xs) = if c == x then drop' cs xs else drop' (c : cs) xs
